﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppNoogle
{
    class Message
    {

        private List<string> subjectList = new List<string>();

        public List<string> SubjectList
        {
            get { return subjectList; }
            set { subjectList = value; }
        }
        private List<string> universityList = new List<string>();
        public List<string> UniversityList
        {
            get { return universityList; }
            set { universityList = value; }
        }
        // This will store the PG or UG
        private string status;
        public string Status
        {
            get { return status; }
            set { status = value; }
        }
        // Method to add a subject in the list
        public void addSubject(string subject)
        {
            SubjectList.Add(subject);
        }
        // Method to add a uni in the list
        public void addUni(string uni)
        {
            UniversityList.Add(uni);
        }
        public string showSubject()
        {
            string s = "";
            for (int i = 0; i < subjectList.Count(); i++)
            {
                s += subjectList[i] + " , ";
            }
            // TODO remove comma at the end
            return s;
        }
        public string showUnis()
        {
            string u = "";
            for (int i = 0; i < universityList.Count(); i++)
            {
                u += universityList[i] + " , ";
            }
            // TODO remove comma at the end
            return u;
        }


    }
}
