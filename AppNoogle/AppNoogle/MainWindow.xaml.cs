﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AppNoogle
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        Noogle n = new Noogle();
        public MainWindow()
        {
            InitializeComponent();
        }
        private void button_Click(object sender, RoutedEventArgs e)
        {

            // MessageBox.Show(n.filtering(textBox.Text));
            if (n.filtering(textBox.Text))
            {
                MessageBox.Show("Message is clear");
                studyLvl.Text = n.fetchStatus();
                subjects.Text = n.fetchSubject();
                unis.Text = n.fetchUnis();
            }
            else
            {
                MessageBox.Show("Message is not clear and has been discarded!");
            }
        }
    }
}
