﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Web.Script.Serialization;

namespace AppNoogle
{
    class Noogle
    {

        List<string> ValidMessages = new List<string>();
        List<string> QMessages = new List<string>();
        List<Message> messageList = new List<Message>();
        public Noogle()
        {

        }


        public bool filtering(string newMsg)
        {
            List<string> curseList = new List<string>();
            curseList = loader(@"C:\textwords.csv");
            string[] tempMessage = newMsg.Split(' ');
            for (int j = 0; j < tempMessage.Count(); j++)
            {
                for (int i = 0; i < curseList.Count(); i++)
                {
                    if (curseList[i] != "")
                    {

                        if (tempMessage[j].ToLower().Equals(curseList[i].ToLower()))
                        {
                            // If it reaches here it means it's not clear
                            QMessages.Add(newMsg);
                            return false;
                        }
                    }
                }
            }
            // If it reaches here it means it's clear
            ValidMessages.Add(newMsg);
            process();
            return true;
        }

        public void process()
        {
            Message msg = new Message();
            // Get the last valid msg
            string s = ValidMessages[ValidMessages.Count() - 1].ToLower();
            // Check for PG or UG
            if (s.Contains("undergraduate") || s.Contains("u/g") || s.Contains("ug"))
            {
                Console.WriteLine("Undergraduate!");
                msg.Status = "Undergraduate";
            }
            else if (s.Contains("postgraduate") || s.Contains("p/g") || s.Contains("pg"))
            {
                Console.WriteLine("Postgraduate");
                msg.Status = "Postgraduate";
            }
            else
            {
                Console.WriteLine("No idea!");
                msg.Status = "Unknown";
            }
            //Check for subject keyword
            // TODO

            // Load uni list
            List<string> uni = new List<string>();
            uni = loader(@"C:\University List.csv");
            // Check for university
            string[] tempMessage = s.Split(' ');
            for (int i = 0; i < uni.Count(); i++)
            {
                if (s.ToLower().Contains(uni[i].ToLower()))
                {
                    msg.addUni(uni[i]);
                    Console.WriteLine(uni[i]);
                }
            }
            // Final step is to store the message to the messages list
            messageList.Add(msg);
            serialization(msg);
        }

        public string fetchStatus()
        {
            return messageList[messageList.Count() - 1].Status;
        }

        public string fetchSubject()
        {
            return messageList[messageList.Count() - 1].showSubject();
        }
        public string fetchUnis()
        {
            return messageList[messageList.Count() - 1].showUnis();
        }


        private List<string> loader(string path)
        {
            List<string> list = new List<string>();
            var reader = new StreamReader(File.OpenRead(path));
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(',');
                list.Add(values[0]);
                // The second column
                // list.Add(values[1]);
            }
            return list;
        }
        public void serialization(Message m)
        {
            // TODO SERIALIZATION OF EACH OBJ TO JSON
            //var json = new JavaScriptSerializer().Serialize(m);
            //Console.WriteLine(json);
        }


    }
}
